﻿using Chatter.Core.ViewModels;
using Chatter.Windows.Views;

namespace Chatter.Windows
{
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = new WindowView(this);
        }
    }
}
