﻿using System.Windows;
using Chatter.Interfaces;
using Chatter.Core.ViewModels;

namespace Chatter.Windows.Views
{
    public class WindowView
    {
        private readonly Window _window;
        private readonly IWindowViewModel _windowViewModel;

        public WindowView(Window window)
        {
            _window = window;
            _windowViewModel = new WindowViewModel();
            _window.StateChanged += (sender, e) =>
            {

            };
        }

        private int _windowRadius = 10;
        private int _outerMarginSize = 10;

        public int ResizeBorder { get; set; } = 6;
        public Thickness ResizeBorderThickness => new Thickness(ResizeBorder);
        public CornerRadius WindowCornerRadius => new CornerRadius(WindowRadius);

        public int OuterMargin
        {
            get => _window.WindowState == WindowState.Maximized ? 0 : _outerMarginSize;
            set => _outerMarginSize = value;
        }

        public int WindowRadius
        {
            get => _window.WindowState == WindowState.Maximized ? 0 : _windowRadius;
            set => _windowRadius = value;
        }
    }
}
